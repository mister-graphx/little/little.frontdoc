<?php namespace Little\FrontDoc ;

use Dflydev\DotAccessData\Data;
use Little\FrontDoc\StyleGuide ;
use Little\FrontDoc\KssParser ;
use Little\FrontDoc\ProjectManager ;
/*
 * class LittleFrontDoc
 */
class FrontDoc{

    protected $router;
    protected $templateEngine;
    protected $assetManager;
    protected $projectManager;

    /** @var \DotAccessData\Data frontdoc config */
    protected $config;
    /** @var \FronDoc\KssParser */
    protected $kssParser;

    public function __construct($config,$events,$router,$templateEngine,$assetManager){
        $this->events = $events;
        $this->router = $router;
        $this->assetManager = $assetManager;
        $this->templateEngine = $templateEngine;
        $this->config = new Data($config);

        $this->projectManager = new ProjectManager();

        $projectConfig = $this->projectManager->get($this->projectManager->project) ;
        $this->project = new Data($projectConfig);
        $this->project->set('namespace',$this->projectManager->project);

        $events->addListener('Kore.loaded',function($event,$container){



            $this->templateEngine->addGlobal('projects',$this->projectManager->projects->export());

            // if ($this->templateEngine->twig_config['cache'] == true ) {
            //     $this->templateEngine->twig_config['cache'] = $_ENV{'STORAGE_PATH'}.'/'.$this->projectManager->project ;
            // }




            $this->templateEngine->addFilter('KssFilter',function($string){
                // Sections links <reference#ref.sub>
                $kssLinkPattern = '/<([\w\d\.#:]*?)>/Uims';
                $string = preg_replace_callback($kssLinkPattern,function($matches){
                    // sub reference case
                    $link = explode('#',$matches[1]);
                    if($section = $this->kssParser->getSection($link[0])){
                        return '<a href="/section/'.$matches[1].'">'.$section->getTitle().'</a>';
                    }
                },$string);

                return $string;
            });

            // $this->assetManager->reset();
            $this->assetManager->config($this->config->get('assets'));
            $this->assetManager->add('frontDoc');
            $this->templateEngine->addGlobal('assets', $this->assetManager->render());



        });

        // Modifier la config des pages pour prendre en compte le Projet
        $this->events->addListener('Jekyll.loaded',function($event,$jekyllConfig){
            $jekyllConfig->set('pages_path', $_ENV{'PUBLIC_PATH'}.'/'.$this->projectManager->project.'/pages/');
        });



        $events->addListener('Kore.afterSetupRoutes', [$this, 'setupRoutes']);
    }
  /**
    * templateEngine setup
    *
    * @param \Little\TemplateEngine twig instance
    */
    public function twigSetUp($templateEngine){
        $templateEngine->addPath($_ENV{'ROOT_PATH'}.'templates/frontdoc','frontdoc');
    }
  /**
    * function setupRoutes
    *
    * add routes
    */
	public function setupRoutes(){

        try {
            $this->kssParser = new KssParser($this->project->get('kss.inspected_paths'));

            $this->templateEngine->addGlobal('project',$this->project->export());


        } catch (\Exception $e) {
            $message = "Erreur dans l'initialisation du KssParser, vérifiez la configuration du projet";
            $this->events->emit('Kore.error404',$message);
        }
        
        $this->router->add('GET', '/preview/{ref}?', function($ref= null){
            $styleguide = new StyleGuide($this->kssParser,$this->project->export('kss'));
            $styleguide->getSection($ref);
            $this->assetManager->reset();
            $this->assetManager->config($this->config->get('assets'));
            $this->assetManager->add('preview');

            $this->templateEngine->addGlobal('preview',$this->assetManager->render());

			return $this->templateEngine->render('@frontdoc/preview',$styleguide->preview());
		});

        $this->router->add('GET', '/code_preview', function(){
            (isset($_GET['ext'])) ? $datas['ext'] = $_GET['ext'] : $datas['ext'] = false;
            (isset($_GET['file'])) ? $datas['file'] = $_GET['file'] : $datas['file'] = false;
            (isset($_GET['ref'])) ? $datas['ref'] = $_GET['ref'] : $datas['ref'] = false;
            try {
                return $this->templateEngine->render('@frontdoc/code_preview',$datas);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
		});

        $this->router->add('GET', '/code_preview/get-file', function(){
            (isset($_GET['file'])) ? $datas['file'] = $_GET['file'] : $datas['file'] = false;
            try {
                return file_get_contents($datas['file']);
            } catch (\Exception $e) {
                echo $e->getMessage();
            }
		});

        $this->router->add('GET', '/livesearch/{search}?', function($search = null){
            $hint = "";
            //lookup all links from ksParser.sections title if length of q>0
            if (strlen($search)>0) {
                $sections = $this->kssParser->getSections();
                foreach($sections as $section){
                    if(stristr($section->getTitle(),$search)===FALSE){
                        $hint .= "";
                    }else{
                        $url = explode('.',$section->getReference());
                        $parent = $url['0'];
                        $hint .= '<a href="/section/'.$parent.'#r'.$section->getReference().'">'.$section->getTitle().'</a><br>';
                        $hint .= '<div>'.$section->getDescription().'</div>';
                    }
                }
            }
            // Set output to "no suggestion" if no hint were found
            // or to the correct values
            if ($hint == "") {
              $response = "no suggestion";
            } else {
              $response = $hint;
            }
            //output the response
            echo $response;
        });

        $this->router->add('GET',  '/section/{ref}?', function($ref= null){
            $styleguide = new StyleGuide($this->kssParser,$this->project->export('kss'));
            $datas['nav'] = $styleguide->navigation();
            $datas['sections'] = $styleguide->getAllSections();
            if(isset($ref)) {
                if($ref=='variables-list'){
                        $scss_variables = new Tools();
                        $vars = $scss_variables->scss_variables_to_json($app::$config['kss']['source_path']);

                        $content = new Tools();
                        $result = $content->bel_env($vars);

                        $datas = array(
                            'content'=> $result
                        );
                }
                else{
                    $section =  $styleguide->getSection($ref);
                    $datas['meta']['title'] = $this->project->get('project.name').' : '.$section['section']['title'];
                    $datas['content'] = $section;
                }
            }else{
                // Front doc Index page
                $readme = $this->projectManager->findReadMe($this->project->get('kss.source_path'),'*.md');
                $datas['meta']['title'] = $this->project->get('project.name').' : Styleguide';
                $datas['content'] = $readme;
            }

            return $this->templateEngine->render('@frontdoc/frontdoc',$datas);
        });
    }



}
