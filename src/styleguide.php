<?php
/**
 * Front Documentor Style Guide utilities
 *
 * Provide method for sending datas to a template engine
*/
namespace Little\FrontDoc ;

/**
 * class StyleGuide
 */
class StyleGuide  {
    protected $config;
    protected $reference;
    protected $section;
    protected $kssParser;

    /**
     * __construct()
     *
     * @param \FrontDoc\kssParser $kss kss parser
     * @param array $kssconfig optional (replacements)
     */
    public function __construct($kssParser, $kssconfig = array()) {
        $this->config = $kssconfig;
        $this->kssParser = $kssParser;
    }
    /**
     * get section by reference
     * @param  string  $ref styleguide reference
     * @return [type]      [description]
     */
    public function getSection($ref){
        $this->reference = (isset($ref) && preg_match('/(\w\.?)+/', $ref)) ? $ref : 'Base';
        try {
            $this->section = $this->kssParser->getSection($this->reference);
            return $this->section();
        } catch (\UnexpectedValueException $e) {
            $this->reference = 'Base';
            $this->section = $this->kssParser->getSection($this->reference);
            return $this->section();
        }
    }

    public function preview(){
        $preview = [
            'section'=> [
                'reference'=>$this->section->getReference(),
                'markup'=>$this->section->getMarkupNormal(),
                'javascript'=>$this->section->getJavascript(),
                'modifiers'=>$this->sectionModifiers($this->section)
            ]
        ];
        return $preview;
    }

    /**
     * style guide tree
     *
     * @return array Style guide tree with all sections infos
     */
    public function getAllSections(){
        foreach($this->kssParser->getTopLevelSections() as $TopLevelSection ){
            $ref = $TopLevelSection->getReference() ;
            $result[$ref] = $this->getSectionInfos($TopLevelSection);
            $result[$ref]['childs'] = $this->listSections($ref);
        }
        return $result ;
    }
    /**
     * styleguide navigation tree
     *
     * @return array structure of the styleguide for building navs
     */
    public function navigation(){
        $sections =array();
        $kss = $this->kssParser;

        $i=0;
        // On Parcours le premier niveau
        foreach ($kss->getTopLevelSections() as $topLevelSection) {

            // On pose l'index du tableau sur la Référence de premier niveau ???
            //$ref = $topLevelSection->getReference();
            $sections[$i]['title'] = $topLevelSection->getTitle();
            //$sections[$i]['description'] = htmlentities($topLevelSection->getDescription());
            $sections[$i]['reference'] =  $topLevelSection->getReference();

            if($kss->getSectionChildren($sections[$i]['reference'])){
                $u=0;
                foreach ($kss->getSectionChildren($sections[$i]['reference']) as $subSection) {
                    if($kss->getSectionChildren($subSection->getReference())){
                            $sections[$i]['childs'][$u]['title'] = $subSection->getTitle();
                            $sections[$i]['childs'][$u]['reference'] = $subSection->getReference();
                            //$sections[$i]['childs'][$u]['description'] = md($subSection->getDescription());

                            $sections[$i]['childs'][$u]['childs'] = $this->listSections($subSection->getReference());
                    }
                    elseif($subSection->getDepth()==1){
                            $sections[$i]['childs'][$u]['title'] = $subSection->getTitle();
                            $sections[$i]['childs'][$u]['reference'] = $subSection->getReference();
                            //$sections[$i]['childs'][$u]['description'] = htmlentities($subSection->getDescription());
                    }

                    $u++;
                }
            }
            $i++;
        }
        return $sections;
    }


    private function section(){
        $styleguide = array(
            'section'=>array(
                "title"=>$this->section->getTitle(),
                "description"=>$this->section->getDescription(),
                "reference"=>$this->section->getReference(),
                'parameters'=> $this->parameters($this->section),
                "sub_sections"=>$this->subsections()
            )
        );

        return $styleguide;
    }


    private function sectionModifiers($section){
        $modifiers=array();
        foreach ($section->getModifiers() as $modifier) {
            $modifiers[]=array(
                'name'=>$modifier->getName(),
                'description'=>$modifier->getDescription(),
                'isExtender'=>$modifier->isExtender(),
                'extendedClass'=>$modifier->getExtendedClass(),
                'exampleHtml'=>$modifier->getExampleHtml()
                );
        }

        return $modifiers;
    }

    private function subsections(){
        $subsections = array();
        foreach ($this->kssParser->getSectionChildren($this->reference) as $subsection) {
            $subsections[]=$this->getSectionInfos($subsection);
        }
        return $subsections;
    }

    /**
     * getSectionInfos
     *
     * @param \Kss\Section $section
     * @return array containing all section infos
    */
    private function getSectionInfos($section){
        $sectionInfos = array(
            'reference'=>$section->getReference(),
            'title'=>$section->getTitle(),
            'description'=>$section->getDescription(),
            'file'=>$section->getFilename(),
            'file_path'=>$section->getFilePath(),
            'parameters'=> $this->parameters($section),
            'compatibility'=>$section->getCompatibility(),
            'experimental'=>$section->getExperimental(),
            'deprecated'=>$section->getDeprecated(),
            'modifiers'=>$this->sectionModifiers($section),
            'markup'=>$this->markupReplacements($section->getMarkupNormal()),
            'markupNormal'=>$this->markupReplacements($section->getMarkupNormal('{class}')),
            'javascriptBlock'=>$section->getJavascript('{class}')
        );

        return $sectionInfos;
    }

  /**
    * markupReplacements
    *
    * applique des transformations sur le markup
    * similaire a $modifierClass qui retourne dans le markup la class du modifier
    * les traitements sont définis dans la config du projet
    * kss->replacements sous la forme d'un tableau expression:remplacement
    *
    * @param string $markup [description]
    * @return [type]         [description]
    */
    private function markupReplacements($markup){
      if(array_key_exists('replacements',$this->config['kss'])
       && $replacements = $this->config['kss']['replacements']
      ){
        foreach($replacements as $expression=>$replacement) {
          $markup = str_replace($expression, $replacement, $markup);
        }
      }
      return $markup;
    }


    private function parameters($section){
        $parameters = array();
        foreach ($section->getParameters() as $parameter){
            $parameters[] = ['name'=>$parameter->getName(),
                             'description'=>$parameter->getDescription()
                             ];
        }
        return $parameters;
    }
    /**
     * return the title and reference of the section childrens
     *
     * @param  \FrontDoc\Section $reference
     * @return array
     */
    private function listSections($reference){
        if($this->kssParser->getSectionChildren($reference)){
            $i=0;
            foreach($this->kssParser->getSectionChildren($reference) as $subSections){
                $result[$i]['title'] = $subSections->getTitle();
                $result[$i]['reference'] = $subSections->getReference();
                if($this->kssParser->getSectionChildren($result[$i]['reference']))
                    $result[$i]['childs'] = $this->listSections($result[$i]['reference']);
                $i++;
            }
            return $result ;
        }
    }





}

?>
