<?php namespace Little\FrontDoc ;

use Dflydev\DotAccessData\Data;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * App
 */
class ProjectManager {
    /** @var \DotAccessData\Data */
    public $projects;
    /** @var string active project */
	public $project;
    // Session used to store the active project
	protected $session;

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param type var Description
     * @return return type
     */
    public function __construct(){
        $this->session = new Session();
		$this->session->start();

        if(! $this->session->has('project')) {
            $this->session->set('project','default');
        }

        // si un projet est passé en param
        if(isset($_GET['project'])){
          $this->session->set('project',$_GET['project']);
        }
        $this->project = $this->session->get('project');

        try {
            $this->projects = new Data();
            $this->projects->import($this->findAllProjects());
        } catch (\Exception $e) {
            echo $e->getMessage();
        }

    }
    /**
     *  get
     *
     * @param string $project - project public folder
     * @return array
     */
    public function get($project){
        return $this->projects->get($project);
    }
  /**
   * findAllProjects()
   *
   * config de projets trouvées dans les dossiers/projets ayant
   * un fichier de config project.json
   *
   * @return array $projects
   */
    public function findAllProjects(){

        try {
            $projects = array();
            $finder = new Finder();
            $finder->files()
                    ->in($_ENV{'PUBLIC_PATH'}.'**/')
                    ->exclude('tmp')
                    ->name('project.json');
            foreach($finder as $file){
                $project_namespace = basename(dirname($file->getRealPath()));
                $projects[$project_namespace] = $this->loadConfig($file->getRealPath()) ;
                $projects[$project_namespace]['project']['namespace'] = $project_namespace;

            }
            return $projects;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
  /**
   * loadConfig
   *
   * Load a json config file and return an array
   *
   * @param string $file path/to/json_file
   * @return array
   *
   */
    public static function loadConfig($file){
        $configFile = file_get_contents($file);
        return json_decode($configFile,TRUE);
    }
    /**
     * search for a Project readme file used for the styleguide index
     * @param  string $path path where finder inspect for a readme file
     * @param  string $file finder expressions or glob pattern
     * @return string error message or file content
     */
    public function findReadMe($path,$file){
        $content = '';
        $finder = new Finder();
        $finder->files()->in($path)->name($file);
        if(iterator_count($finder)>0){
            foreach($finder as $item){
                $content = $item->getContents();
            }
        }else{
            $content ="aucun resultat pour : ".$path.$file;
        }
        return $content;
    }


}
