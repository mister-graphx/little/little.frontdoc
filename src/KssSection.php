<?php

/**
 * Section
 *
 * A KSS Comment Block that represents a single section containing a description,
 * modifiers, and a section reference.
 */

namespace Little\FrontDoc ;

use Kss\Section;

class KssSection extends Section
{


    /**
     * The parsed javascript comment in the KSS Block
     *
     * @var string
     */
    protected $javascript = null;

  /**
    * Returns the source path for where the comment block was located
    *
    * @return string
    */
    public function getFilePath()
    {
        if ($this->file === null) {
            return '';
        }

        return $this->file->getPath();
    }

    /**
     * Returns the description for the section
     *
     * @return string
     */
    public function getDescription()
    {
        $descriptionSections = array();

        foreach ($this->getCommentSections() as $commentSection) {
            // Anything that is not the section comment or modifiers comment
            // must be the description comment
            if ($commentSection != $this->getReferenceComment()
                && $commentSection != $this->getTitleComment()
                && $commentSection != $this->getMarkupComment()
                && $commentSection != $this->getJavascriptComment()
                && $commentSection != $this->getDeprecatedComment()
                && $commentSection != $this->getExperimentalComment()
                && $commentSection != $this->getCompatibilityComment()
                && $commentSection != $this->getModifiersComment()
                && $commentSection != $this->getParametersComment()
            ) {
                $descriptionSections[] = $commentSection;
            }
        }

        return implode("\n\n", $descriptionSections);
    }

    /**
     * Returns the markup defined in the section
     * or include a source file if specified
     *
     * @return string
     */
    public function getMarkup()
    {
        if ($this->markup === null) {
            if ($markupComment = $this->getMarkupComment()) {
                // Identify the markup comment by the Markup: marker
                // and optionaly a source file
                if (preg_match('/^\s*Markup:([\s\w\.\/_-]*\.[\w]*)?/i', $markupComment, $matches)) {
                    // file source or markup
                    if(isset($matches[1]) ){
                      $file= file_get_contents(realpath(trim($matches[1])));
                      $this->markup = $file;
                    }else{
                      $this->markup = trim(preg_replace('/\s*Markup:/i','',$markupComment));
                    }
                }
            }
        }

        return $this->markup;
    }

    /**
     * Returns the Javascript defined in the section
     *
     * @return string
     */
    public function getJavascript()
    {
        if ($this->javascript === null) {
            if ($javascriptComment = $this->getJavascriptComment()) {
                $this->javascript = trim(preg_replace('/^\s*Javascript:/i', '', $javascriptComment));
            }
        }

        return $this->javascript;
    }


    /**
     * Returns a boolean value regarding the presence of javascript in the kss-block
     *
     * @return boolean
     */
    public function hasJavascript()
    {
        return $this->getJavascript() !== null;
    }


    /**
     * Returns the part of the KSS Comment Block that contains the javascript
     *
     * @return string
     */
    protected function getJavascriptComment()
    {
        $javascriptComment = null;

        foreach ($this->getCommentSections() as $commentSection) {
            // Identify the javascript comment by the Javascript: marker
            if (preg_match('/^\s*Javascript:/i', $commentSection)) {
                $javascriptComment = $commentSection;
                break;
            }
        }

        return $javascriptComment;
    }


}
