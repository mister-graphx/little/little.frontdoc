/**
@function autoResize
@desc Concat two or more strings
@author Mist. GraphX mistergraphx@gmail.com
@param {int} id block id to be resized
@return String
*/
function autoResize(id){
    var newheight;
    var newwidth;

    if(document.getElementById){
        newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
        newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth;
    }
    document.getElementById(id).style.height= (newheight) + "px";
    if (cookieData.element=='auto-adjust') {
        document.getElementById(id).style.width= "100%";
        console.log(cookieData.element);
    }else{
        document.getElementById(id).style.width= (newwidth) + "px";
    }
}

/** #resizeTo

used to resize bloc.id & simulate a wiewport viewport

Markup:
<li>
    <span   id="auto-adjust"
            onclick="resizeTo('iframe.preview','auto','auto',this);"
            data-title="Auto Adjust">
    <i class="icn-resize-horizontal small"></i>
 </span>
</li>

@function resizeTo
@desc used to resize bloc.id & simulate a wiewport viewport
@param {string} selector bloc #id
@param {int} width 
@param {int} height
@param {domElements} this (the DOM element )


*/

function resizeTo(selector,newWidth,newHeight,spanId) {
    
    var border = "1";
    var Frame = document.querySelectorAll(selector);
    
    if (newWidth=='auto'||newHeight=='auto') {
        for (var i = 0; i < Frame.length; i++) {
            Frame[i].style.width = "100%";
        }
    }else{
        for (var i = 0; i < Frame.length; i++) {
            Frame[i].style.height= (+newHeight-(border*2)) + "px";
            Frame[i].style.width= (+newWidth+(15-(border*2))) + "px";
            Frame[i].style.border = border+"px solid silver";
        }
    }
    
    // Adding informations about The selected viewport in top navigation
    parent = document.getElementById("styleGuideTools");
    
    if (document.getElementById('viewport')==undefined) {
        var info = document.createElement('div');
        info.id ="viewport";
    }else{
        info = document.getElementById("viewport");
    }
    
    liste = parent.lastChild;
    parent.insertBefore(info,liste);
    
    if (spanId==undefined) {
        span = document.getElementById(cookieData.element);
    }else{
        span = spanId;
    }
    
    viewport = span.getAttribute('data-title');
    // Set innerHtml from the Data-title attribute on Span selected
    document.getElementById('viewport').innerHTML = viewport; 
    
    // Scroll to anchor after resizing frames
    var hash = location.hash;
    if (hash==null) {
        var blockId = document.getElementById(hash.substr(1)).offsetTop;
        window.scrollTo(0, blockId);
    }
    
    // Store informations about the viewport testing size
    cookieData.viewport = [newWidth,newHeight];
    cookieData.element = span.id;    
    createCookie(cookieName,JSON.stringify(cookieData),7);
    
    // span.active  
    if (parent.getElementsByClassName('active').length > 0 ) {
        var activeSpans = parent.getElementsByClassName('active');
        for (var i=0; i<activeSpans.length;i++) {
            activeSpans[i].classList.remove('active');
        }
        span.classList.add('active');
    }else{
        span.classList.add('active');
    }
    
}

/* #createCookie() & readCookie()

<http://www.quirksmode.org/js/cookies.html>

Styleguide js.Cookies
*/
function createCookie(name,value,days) {
        if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
}

function eraseCookie(name) {
	createCookie(name,"",-1);
}

// Top-nav project-switcher
function switchProject() {
    var x = document.getElementById("project-switcher").value;
    window.location.assign("?project="+x);
}




// ---------------------------------------
// INIT
// ---------------------------------------

// Highlight.js init
hljs.initHighlightingOnLoad();

var cookieName = "styleguide";

// Cookie
if (document.cookie.indexOf(cookieName)==-1) {
    var cookieData = {};
    cookieData.viewport = "100%";
    createCookie(cookieName,JSON.stringify(cookieData),7);
}else{
    var cookieData = JSON.parse(readCookie(cookieName));
    resizeTo('iframe',cookieData.viewport[0],cookieData.viewport[1]);
}

 
document.getElementById("livesearch").style.visibility="hidden";


