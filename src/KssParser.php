<?php

/**
 * KssParser
 *
 * Accepts an array of directories and parses them stylesheet files present in
 * them for KSS Comment Blocks
 *
 * @extends Sacn\Kss\Parser
 */

namespace Little\FrontDoc ;

use Symfony\Component\Finder\Finder;
use Kss\Exception\UnexpectedValueException;
use Kss\Parser;

class KssParser extends Parser
{
    /**
     * Adds a section to the Sections collection
     *
     * @param string $comment
     * @param \splFileObject $file
     */
    protected function addSection($comment, \splFileObject $file)
    {
        $section = new KssSection($comment, $file);
        $this->sections[$section->getReference(true)] = $section;
        $this->sectionsSortedByReference = false;
    }

    /**
     * Returns a Section object matching the requested reference. If reference
     * is not found, an empty Section object is returned instead
     *
     * @param string $reference
     *
     * @return Section
     *
     * @throws UnexepectedValueException if reference does not exist
     */
    public function getSection($reference)
    {
        $reference = KssSection::trimReference($reference);
        $reference = strtolower(KssSection::normalizeReference($reference));

        foreach ($this->sections as $sectionKey => $section) {
            $potentialMatch = strtolower(KssSection::normalizeReference($sectionKey));
            if ($reference === $potentialMatch) {
                return $section;
            }
        }

        throw new UnexpectedValueException('Section with a reference of ' . $reference . ' cannot be found!');
    }



}
